Pirata, loro, pirata
====================

Buscador de tesoros
-------------------

~~~
wget $(sudo buscar-tesoro wlan0)
~~~

Correr esto y luego abrir un navegador en la pagina que queremos buscar el tesoro, ejemplo: una pagina con un video 
"imposible" de descargar y ¡vualà!, se comienza a descargar. Para correr sin sudo podes agregar `%sudo 
ALL=(root)NOPASSWD:/usr/sbin/urlsnarf` al archivo _/etc/sudores_.

Descargar una pagina completa
-----------------------------

~~~
web-dl http://maquinaslibres.noblogs.org/
~~~


Compartir un archivo en torrent
-------------------------------

~~~
2torrent archivo.ogg
~~~~

Busca blogs sobre un tema en DuckDuckGo
--------------------------------------

~~~
vblog-dl blog "gumball en latino"
~~~

_No descarga el contenido, solo busca blogs con ese tema_

Descargar todos los videos de un blog
-------------------------------------

~~~
vblog-dl http://gumballenlatino.blogspot.com.ar/
~~~

Descarga masiva sobre un tema
------------------------------

~~~
vblog-dl blog "gumball en latino" | while read U; do vblog-dl $U; done
~~~

_Busca blogs sobre un tema y descarga todos los videos_

Busca películas en DuckDuckGo
-----------------------------

~~~
vblog-dl "'Amici Miei' ver online"
~~~

Buscar torrent y magnet
-----------------------

~~~
torrent-dl "Amici Miei" movie
~~~

para descargar una pelicula

Buscar y descargar videos de youtube
------------------------------------

~~~
ptube "velvet underground"
~~~

_Esto descarga una lista de video de la Velvet Underground_

Buscar y descargar capítulos y temporadas de videos en youtube
--------------------------------------------------------------

~~~
userie -c 1-100 "Un Show Más Capítulo %c"
~~~

_Esto descarga los capitulos del 1 al 100 de un show más, si puede..._


Hacer un copia de los archivos ya vistos (en flash)
---------------------------------------------------

~~~
flashproc
~~~

_Va a ser una copia de todos los videos y audios que tenga en memoria_ 

Lista de quehaceres
-------------------

- hacer todo con enlaces
- hacer que corra en cualquier sistema (xdg-user-dir)
- tutoriales de como comprartir esta información en p2p
- Muchas cosas
